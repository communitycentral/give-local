import { Time } from '@angular/common';
import { Charity } from './charity';

export class ApiCharityResponse {
  Success: boolean;
  Count: number;
  ResponseTime: Time;
  Message: string;
  Data: Charity[];
}
