import { Charity } from './charity';

export const Charities: Charity[] = [
  { LegalName: 'RSPCA', ABN: '35 730 738 037' },
  { LegalName: 'Anglicare', ABN: '25 372 194 327' }
];
