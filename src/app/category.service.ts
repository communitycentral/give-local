import { Injectable } from '@angular/core';
import { Category } from './category';
import { Categories } from './categories';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private messageService: MessageService) { }

  getCategories(): Observable<Category[]> {
    // TODO: send message _after_ fetching the categories
    this.messageService.add('CategoryService: fetched categories');
    return of(Categories);
  }
}
