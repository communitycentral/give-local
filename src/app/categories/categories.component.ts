import { Component, OnInit } from '@angular/core';
import { Categories } from '../categories';
import { CategoryService } from '../category.service';
import { CharityService } from '../charity.service';
import { Charity } from '../charity';
import { Category } from '../category';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  categories: Category[] = [];
  charities: Charity[] = [];

  constructor(private categoryService: CategoryService, private charityService: CharityService) { }

  ngOnInit() {
    this.getCategories();
  }

  getCategories(): void {
    this.categoryService.getCategories()
      .subscribe(categories => this.categories = categories);
  }

  getCharitiesByCategory(category: string): void {
    this.charityService.getCharitiesByCategory(category, 'QLD')
      .subscribe(charities => this.charities = charities.Data);
  }
}
