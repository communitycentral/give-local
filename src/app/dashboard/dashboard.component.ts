import { Component, OnInit } from '@angular/core';
import { Charity } from '../charity';
import { CharityService } from '../charity.service';
import { Category } from '../category';
import { CategoryService } from '../category.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  charities: Charity[] = [];
  categories: Category[] = [];

  constructor(private charityService: CharityService) { }

  ngOnInit() {
  }
}
