import { Injectable } from '@angular/core';
import { Charity } from './charity';
import { Charities } from './charities';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { ApiCharityResponse } from './api-charity-response';

@Injectable({
  providedIn: 'root'
})
export class CharityService {

  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) { }

  private baseUrl = 'https://www.mcdapi.com/api/v1/charities/';

  getCharitiesByCategory(category: string, states: string): Observable<ApiCharityResponse> {
    // TODO: send message _after_ fetching the charities
    this.log('fetching charities');
    return this.http.get<ApiCharityResponse>(`${this.baseUrl}beneficiary/${category}/${states}`)
      .pipe(
        tap(response => this.log('fetched charities')),
        catchError(this.handleError('getCharitiesByCategory', new ApiCharityResponse))
      );
  }

  private log(message: string) {
    this.messageService.add(`CharityService: ${message}`);
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
