import { Component, OnInit } from '@angular/core';
import { Charities } from '../charities';
import { CharityService } from '../charity.service';
import { Charity } from '../charity';

@Component({
  selector: 'app-charities',
  templateUrl: './charities.component.html',
  styleUrls: ['./charities.component.css']
})
export class CharitiesComponent implements OnInit {

  charities = Charities;
  selectedCharity: Charity;

  constructor(private charityService: CharityService) { }

  ngOnInit() {
    this.getCharitiesByCategory('Animals');
  }

  getCharitiesByCategory(category: string): void {
    this.charityService.getCharitiesByCategory(category, 'QLD')
      .subscribe(charities => this.charities = charities.Data);
  }

  onSelect(charity: Charity): void {
    this.selectedCharity = charity;
  }
}
