import { Component, OnInit, Input } from '@angular/core';
import { Charity } from '../charity';

@Component({
  selector: 'app-charity-details',
  templateUrl: './charity-details.component.html',
  styleUrls: ['./charity-details.component.css']
})
export class CharityDetailsComponent implements OnInit {

  @Input() charity: Charity;

  constructor() { }

  ngOnInit() {
  }

}
